create table patients(
       id number not null primary key,
       first_name varchar2(25) not null,
       last_name varchar2(25) not null,
       mother_name varchar2(100) not null,
       sex varchar2(8),
       date_of_birth date not null,
       date_of_death date,
       place_of_birth varchar2(50) not null,
       phone_number varchar2(20) not null,
       address varchar2(100) not null,
       email_address varchar2(100)
);

create table relationships(
       id number not null primary key,
       first_patient number not null,
       second_patient number not null,
       type varchar2(20),
       quality varchar2(10),
       distance number,
       start_date date not null,
       end_date date
);


insert into patients(id,
                     first_name,
                     last_name,
                     mother_name,
                     sex,
                     date_of_birth,
                     date_of_death,
                     place_of_birth,
                     phone_number,
                     address,
                     email_address)
       values(1, 'Tomi', 'Tomi', 'Example name', 'male', to_date('17-11-2000', 'dd-mm-yyyy'), null, 'P�cs', '+012345678', '7632 P�cs Aidinger J�nos �t 13.', 'example@example.com');
                     
                     
insert into patients(id,
                     first_name,
                     last_name,
                     mother_name,
                     sex,
                     date_of_birth,
                     date_of_death,
                     place_of_birth,
                     phone_number,
                     address,
                     email_address)
       values(2, 'Gabor', 'Gabor', 'Mother name', 'male', to_date('02-11-2000', 'dd-mm-yyyy'), null, 'Szigetv�r', '+0123443678', '7900 Szigetv�r Szabads�g utca 1', 'example1@example.com');
       

insert into patients(id,
                     first_name,
                     last_name,
                     mother_name,
                     sex,
                     date_of_birth,
                     date_of_death,
                     place_of_birth,
                     phone_number,
                     address,
                     email_address)
       values(3, 'Gipsz', 'Jakab', 'Random Name', 'male', to_date('02-03-1989', 'dd-mm-yyyy'), null, 'Budapest', '+0101443678', '7632 P�cs Boszork�ny �t 2', 'example2@example.com');
       
       
       
insert into patients(id,
                     first_name,
                     last_name,
                     mother_name,
                     sex,
                     date_of_birth,
                     date_of_death,
                     place_of_birth,
                     phone_number,
                     address,
                     email_address)
       values(4, 'Kiss', 'Piroska', 'Name Name', 'female', to_date('12-01-1956', 'dd-mm-yyyy'), null, 'Lad', '+0125673678', '7900 Szigetv�r Alm�s utca 18', 'example3@example.com');
       
       
insert into relationships(id,
                          first_patient,
                          second_patient,
                          type,
                          quality,
                          distance,
                          start_date,
                          end_date)
       values(1, 1, 2, 'friend', 'positive', 1, to_date('01-04-2018', 'dd-mm-yyyy'), null);
insert into relationships(id,
                          first_patient,
                          second_patient,
                          type,
                          quality,
                          distance,
                          start_date,
                          end_date)
       values(2, 1, 3, 'colleague', 'positive', 5, to_date('10-09-2020', 'dd-mm-yyyy'), null);

insert into relationships(id,
                          first_patient,
                          second_patient,
                          type,
                          quality,
                          distance,
                          start_date,
                          end_date)
       values(3, 2, 3, 'colleague', 'positive', 3, to_date('09-05-2017', 'dd-mm-yyyy'), null);
       
insert into relationships(id,
                          first_patient,
                          second_patient,
                          type,
                          quality,
                          distance,
                          start_date,
                          end_date)
       values(4, 1, 4, 'neighbour', 'negative', 7, to_date('09-08-2015', 'dd-mm-yyyy'), null);
            
Alter table relationships
add constraint same_patient_check check (first_patient <> second_patient);
       
       
select rowid, p.* from patients p;
select rowid, r.* from relationships r;
       
