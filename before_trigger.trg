CREATE OR REPLACE TRIGGER before_trigger
BEFORE UPDATE or insert
   ON relationships
   FOR EACH ROW
declare
   v_birth_date date;

BEGIN
  select p.date_of_birth into v_birth_date from patients p where p.id = :new.first_patient; 
   if :new.start_date < v_birth_date
     then
       :new.start_date := v_birth_date;
   end if;
   
  select p.date_of_birth into v_birth_date from patients p where p.id = :new.second_patient; 
   if :new.start_date < v_birth_date
     then
       :new.start_date := v_birth_date;
   end if;
END;
/
