----------------------------------
-- 1. Create user, add grants   --
----------------------------------
DECLARE
  v_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO v_count FROM dba_users t WHERE t.username='HR_MANAGER';
  IF v_count = 1 THEN 
    EXECUTE IMMEDIATE 'DROP USER hr_manager CASCADE';
  END IF;
END;
/
CREATE USER hr_manager 
  IDENTIFIED BY "12345678" 
  DEFAULT TABLESPACE users
  QUOTA UNLIMITED ON users
;

GRANT CREATE TRIGGER TO hr_manager;
GRANT CREATE SESSION TO hr_manager;
GRANT CREATE TABLE TO hr_manager;
GRANT CREATE VIEW TO hr_manager;
GRANT CREATE SEQUENCE TO hr_manager;
GRANT CREATE PROCEDURE TO hr_manager;
GRANT CREATE TYPE TO hr_manager;
