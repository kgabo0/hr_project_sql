CREATE OR REPLACE TRIGGER after_trigger
AFTER UPDATE
   OF date_of_death
   ON patients
   FOR EACH ROW
BEGIN
  update relationships set end_date = :new.date_of_death where :new.id = first_patient or :new.id = second_patient;
END;
/
